import "../styleLoad";
import { submitForm } from "../core/signal_common";
import { SignalApi } from "../core/signal_api";
import io from "socket.io-client"
const body = document.querySelector('body'),
    sidebar = body.querySelector('nav'),
    toggle = body.querySelector(".toggle");
toggle.addEventListener("click", () => {
    sidebar.classList.toggle("close");
})

const initialize = async () => {
    loadPage();
    const socket = io.connect("http://localhost:7000");
    socket.on('studentTestAdded', () => {
        loadPage();
        console.log("add thanh cong")
    });
    socket.on('studentTestEdited', () => {
        loadPage();
        console.log("edit thanh cong")
    });
    socket.on('studentTestDeleted', () => {
        loadPage();
        console.log("delete thanh cong")
    });
    function loadPage() {
        let studentIdSearch = $("#studentId-Search").val();
        let testIdSearch = $("#testId-Search").val();
        SignalApi.StudentTest.getStudentTest(
            {
                StudentId: studentIdSearch,
                TestId: testIdSearch,
            }
        ).then((data) => {
            var template = $("#studentTestTemplate").html()
            var div = ""
            for (const [index, obj] of data.data.entries()) {
                div += template.interpolate({
                    stt: index + 1,
                    studentId: obj.Students ? obj.Students.Id : "",
                    testId: obj.Tests ? obj.Tests.Id : "",
                    score: obj.Score,
                })
            }
            $("#showData").html(div)
        })
            .catch((error) => {
                console.log(error)
            })
    }
    $(document).on('click', "#searchStudentTest", async function () {
        loadPage()
    })


    $(document).on('click', "#addStudentTest", async function () {
        try {
            var addStudentTest = () => {

                let studentId = $("#add-studentId").val();
                let testId = $("#add-testId").val();
                let score = $("#add-score").val();

                SignalApi.StudentTest.addStudentTest({
                    StudentId: studentId,
                    TestId: testId,
                    Score: score
                }).then((data) => {
                    if (!data.status) {
                        alert(data.message);
                        return;
                    }
                    if (data.status) {
                        $("#modalAdd").modal('hide');
                        socket.emit('newStudentTestAdded', data);
                        loadPage();
                    }
                });
            };
            submitForm(addStudentTest);
        } catch (error) {
            console.log(error);
        }
    });


    $(document).on('click', "#editStudentTest", async function () {
        try {
            var editStudentTest = () => {
                let studentId = $("#edit-studentId").val();
                let testId = $("#edit-testId").val();
                let score = $("#edit-score").val();

                SignalApi.StudentTest.updateStudentTest({
                    StudentId: studentId,
                    TestId: testId,
                    Score: score,
                }).then((data) => {
                    if (!data.status) {
                        alert(data.message);
                        return;
                    }
                    if (data.status) {
                        $("#modalEdit").modal('hide');
                        socket.emit('newStudentTestEdited', data);
                        loadPage();
                    }
                });
            };
            submitForm(editStudentTest);
        } catch (error) {
            console.log(error);
        }
    });
    $(document).on('click', "#deleteStudentTest", async function () {
        try {
            if (confirm('Are you sure delete?')) {
                let idStudent = $(this).data('studentid')
                let idTest = $(this).data('testid')
                SignalApi.StudentTest.deleteStudentTest({
                    StudentId: idStudent,
                    TestId: idTest
                }).then((data) => {
                    if (!data.status) {
                        console.log(data.message);
                        alert(data.message)
                        return;
                    }
                    if (data.status) {
                        socket.emit('newStudentTestDeleted', data);
                        loadPage();
                    }
                })
            }
        } catch (error) {
            console.log(error)
        }
    });

    $('#modalEdit').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var studentId = button.data('studentid')
        var testId = button.data('testid')
        var score = button.data('score')
        var modal = $(this)
        modal.find('#edit-studentId').val(studentId);
        modal.find('#edit-testId').val(testId);
        modal.find('#edit-score').val(score);
    })
}


window.addEventListener('DOMContentLoaded', initialize)
