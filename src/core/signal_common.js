const ajax = {
    post: (t) => {
        $.ajax({
            url: t.url,
            headers: t.headers,
            type: "POST",
            data: t.data,
            contentType: t.contentType ?? "application/json",
            processData: false,
            success: function (data) {
                if (t.success) t.success(data);
            },
            error: function (data) {
                if (t.error) t.error(data);
            }
        });
    },
    postFormData: (t) => {
        $.ajax({
            url: t.url,
            headers: t.headers,
            type: "POST",
            enctype: 'multipart/form-data',
            data: t.data,
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                if (t.success) t.success(data);
            },
            error: function (data) {
                if (t.error) t.error(data);
            }
        });
    },
    get: (t) => {
        $.ajax({
            url: t.url,
            headers: t.headers,
            type: "GET",
            success: function (data) {
                if (t.success) t.success(data);
            },
            error: function (data) {
                if (t.error) t.error(data);
            }
        });
    },
};
exports.ajax = ajax

String.prototype.interpolate = function (o) {
    return this.replace(/{([^{}]*)}/g, function (_a, b) {
        var c = b.trim();
        var r = o[c];
        return typeof r === "string" || typeof r === "number" ? r : "";
    });
}

var formAction;
function submitForm(action) {
    formAction = action
};
$(".needs-validation").on("submit", function (event) {
    if (!this.checkValidity()) {
        event.preventDefault();
        event.stopPropagation();
        this.classList.add("was-validated");
        return;
    }
    event.preventDefault();
    if (formAction != undefined) {
        formAction()
    }
    this.classList.remove("was-validated");
});
exports.submitForm = submitForm


exports.cookie = {

    setCookie: (cname, cvalue, exdays) => {
        exdays == undefined ? exdays = 1 : exdays
        const d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        let expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    },
    getCookie: (cname) => {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
}
function getUserRole(JWT) {
    if (JWT != "") {
        let jwtData = JWT.split('.')[1]
        let decodedJwtJsonData = window.atob(jwtData)
        const obj = JSON.parse(decodedJwtJsonData);
        return obj.role;
    } else {
        return null;
    }

}
exports.getUserRole = getUserRole

function checkTimeOutJwt(JWT) {
    let jwtData = JWT.split('.')[1]
    let decodedJwtJsonData = window.atob(jwtData)
    const obj = JSON.parse(decodedJwtJsonData);
    let nowTime = Date.now() / 1000
    return obj.exp >= nowTime;
}
exports.checkTimeOutJwt = checkTimeOutJwt

function authentication(role) {
    if (role == 'User') {
        window.location = "../index.html"
        return;
    }
}
exports.authentication = authentication