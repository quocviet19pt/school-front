import "../styleLoad";
import { submitForm } from "../core/signal_common";
import { SignalApi } from "../core/signal_api";
import io from "socket.io-client"
const body = document.querySelector('body'),
    sidebar = body.querySelector('nav'),
    toggle = body.querySelector(".toggle");
toggle.addEventListener("click", () => {
    sidebar.classList.toggle("close");
})

const initialize = async () => {
    loadPage();
    const socket = io.connect("http://localhost:7000");
    socket.on('teacherAdded', () => {
        loadPage();
        console.log("add thanh cong")
    });
    socket.on('teacherEdited', () => {
        loadPage();
        console.log("edit thanh cong")
    });
    socket.on('teacherDeleted', () => {
        loadPage();
        console.log("delete thanh cong")
    });

    function loadPage() {
        let codeSearch = $("#teacherCode-Search").val();
        let nameSearch = $("#teacherName-Search").val();
        let expSearch = $("#teacherExp-Search").val();
        SignalApi.Teacher.getTeacher(
            {
                Code: codeSearch,
                Name: nameSearch,
                Exp: expSearch
            }
        ).then((data) => {
            var template = $("#teacherTemplate").html()
            var div = ""
            for (const [index, obj] of data.data.entries()) {
                div += template.interpolate({
                    stt: index + 1,
                    id: obj.Id,
                    code: obj.Code,
                    name: obj.Name,
                    exp: obj.Exp,
                    image: obj.Image,
                    subjectId: obj.Subjects ? obj.Subjects.Id : "",
                })
            }
            $("#showData").html(div)
        })
            .catch((error) => {
                console.log(error)
            })
    }
    $(document).on('click', "#searchTeacher", async function () {
        loadPage()
    })


    $(document).on('click', "#addTeacher", async function () {
        try {
            var addTeacher = () => {

                let code = $("#add-teacherCode").val();
                let name = $("#add-teacherName").val();
                let exp = $("#add-teacherExp").val();
                let image = $("#add-teacherImage").val();
                let subjectId = $("#add-teacherSubjectId").val();

                SignalApi.Teacher.addTeacher({
                    Code: code,
                    Name: name,
                    Exp: exp,
                    Image: image,
                    SubjectId: subjectId
                }).then((data) => {
                    if (!data.status) {
                        alert(data.message);
                        return;
                    }
                    if (data.status) {
                        $("#modalAdd").modal('hide');
                        $("#add-teacherCode").val("");
                        $("#add-teacherName").val("");
                        $("#add-teacherExp").val("");
                        $("#add-teacherImage").val("");
                        $("#add-teacherSubjectId").val("");
                        socket.emit('newTeacherAdded', data);
                        loadPage();
                    }
                });
            };
            submitForm(addTeacher);
        } catch (error) {
            console.log(error);
        }
    });


    $(document).on('click', "#editTeacher", async function () {
        try {
            var editTeacher = () => {
                let id = $("#edit-teacherId").val();
                let code = $("#edit-teacherCode").val();
                let name = $("#edit-teacherName").val();
                let exp = $("#edit-teacherExp").val();
                let image = $("#edit-teacherImage").val();
                let subjectId = $("#edit-teacherSubjectId").val();

                SignalApi.Teacher.updateTeacher({
                    Id: id,
                    Code: code,
                    Name: name,
                    Exp: exp,
                    Image: image,
                    SubjectId: subjectId
                }).then((data) => {
                    if (!data.status) {
                        alert(data.message);
                        return;
                    }
                    if (data.status) {
                        $("#modalEdit").modal('hide');
                        socket.emit('newTeacherEdited', data);
                        loadPage();
                    }
                });
            };
            submitForm(editTeacher);
        } catch (error) {
            console.log(error);
        }
    });
    $(document).on('click', "#deleteTeacher", async function () {
        try {
            if (confirm('Are you sure delete?')) {
                let idBot = $(this).data('id')
                SignalApi.Teacher.deleteTeacher({
                    Id: idBot,
                }).then((data) => {
                    if (!data.status) {
                        console.log(data.message);
                        alert(data.message)
                        return;
                    }
                    if (data.status) {
                        socket.emit('newTeacherDeleted', data);
                        loadPage();
                    }
                })
            }
        } catch (error) {
            console.log(error)
        }
    });

    $('#modalEdit').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var code = button.data('code')
        var name = button.data('name')
        var exp = button.data('exp')
        var image = button.data('image')
        var subjectId = button.data('subjectid')
        var modal = $(this)
        modal.find('#edit-teacherId').val(id);
        modal.find('#edit-teacherCode').val(code);
        modal.find('#edit-teacherName').val(name);
        modal.find('#edit-teacherExp').val(exp);
        modal.find('#edit-teacherImage').val(image);
        modal.find('#edit-teacherSubjectId').val(subjectId)
    })
}


window.addEventListener('DOMContentLoaded', initialize)
