import "../styleLoad";
import { submitForm } from "../core/signal_common";
import { SignalApi } from "../core/signal_api";
import moment from "moment/moment";
import io from 'socket.io-client';
const body = document.querySelector('body'),
    sidebar = body.querySelector('nav'),
    toggle = body.querySelector(".toggle");
toggle.addEventListener("click", () => {
    sidebar.classList.toggle("close");
})


const initialize = async () => {
    loadPage();
    const socket = io.connect("http://localhost:7000");
    socket.on('studentAdded', () => {
        loadPage();
        console.log("add thanh cong")
    });
    socket.on('studentEdited', () => {
        loadPage();
        console.log("edit thanh cong")
    });
    socket.on('studentDeleted', () => {
        loadPage();
        console.log("delete thanh cong")
    });

    function loadPage() {
        let codeSearch = $("#studentCode-Search").val();
        let nameSearch = $("#studentName-Search").val();
        SignalApi.Student.getStudent(
            {
                Code: codeSearch,
                Name: nameSearch,
            }
        ).then((data) => {
            var template = $("#studentTemplate").html()
            var div = ""
            for (const [index, obj] of data.data.entries()) {
                var formattedBirth = moment(obj.Birth).format("DD/MM/YYYY");
                var formattedBirthModal = moment(obj.Birth).format("MM/DD/YYYY");
                div += template.interpolate({
                    stt: index + 1,
                    id: obj.Id,
                    code: obj.Code,
                    name: obj.Name,
                    birth: formattedBirth,
                    birthModal: formattedBirthModal,
                    image: obj.Image,
                })
            }
            $("#showData").html(div)
        })
            .catch((error) => {
                console.log(error)
            })
    }

    $(document).on('click', "#searchStudent", async function () {
        loadPage()
    })

    // $(document).on('click', "#addStudent", async function () {
    //     try {
    //         var addStudent = () => {

    //             let code = $("#add-studentCode").val();
    //             let name = $("#add-studentName").val();
    //             let birth = $("#add-studentBirth").val();
    //             let image = $("#add-studentImage")[0].files[0];
    //             let formData = new FormData();
    //             formData.append('Image', image);

    //             // Thêm các trường dữ liệu khác vào formData
    //             formData.append('Code', code);
    //             formData.append('Name', name);
    //             formData.append('Birth', birth);

    //             SignalApi.Student.addStudent(formData).then((data) => {
    //                 if (!data.status) {
    //                     alert(data.message);
    //                     return;
    //                 }
    //                 if (data.status) {
    //                     $("#modalAdd").modal('hide');
    //                     // socket.emit("newStudentAdded");
    //                     loadPage();
    //                 }
    //             });
    //         };
    //         submitForm(addStudent);
    //     } catch (error) {
    //         console.log(error);
    //     }
    // });

    $(document).on('click', "#addStudent", async function () {
        try {
            var addStudent = () => {
                let code = $("#add-studentCode").val();
                let name = $("#add-studentName").val();
                let birth = $("#add-studentBirth").val();
                let image = $("#add-studentImage")[0].files[0];
                let formData = new FormData();
                formData.append('Code', code);
                formData.append('Name', name);
                formData.append('Birth', birth);
                formData.append('Image', image);
                SignalApi.Student.addStudent(
                    formData
                    // Code: code,
                    // Name: name,
                    // Birth: birth,
                    // Image: image
                ).then((data) => {
                    if (!data.status) {
                        alert(data.message);
                        return;
                    }
                    if (data.status) {
                        $("#modalAdd").modal('hide');
                        $("#add-studentCode").val("");
                        $("#add-studentName").val("");
                        $("#add-studentBirth").val("");
                        $("#add-studentImage").val("");
                        socket.emit('newStudentAdded', data);
                        loadPage();
                    }
                });
            };
            submitForm(addStudent);
        } catch (error) {
            console.log(error);
        }
    });

    $(document).on('click', "#editStudent", async function () {
        try {
            var editStudent = () => {
                let id = $("#edit-studentId").val();
                let code = $("#edit-studentCode").val();
                let name = $("#edit-studentName").val();
                let birth = $("#edit-studentBirth").val();
                let image = $("#edit-studentImage")[0].files[0];
                let formData = new FormData();
                formData.append('Id', id);
                formData.append('Code', code);
                formData.append('Name', name);
                formData.append('Birth', birth);
                formData.append('Image', image);

                SignalApi.Student.updateStudent(
                    formData
                ).then((data) => {
                    if (!data.status) {
                        alert(data.message);
                        return;
                    }
                    if (data.status) {
                        $("#modalEdit").modal('hide');
                        socket.emit('newStudentEdited', data);
                        loadPage();
                    }
                });
            };
            submitForm(editStudent);
        } catch (error) {
            console.log(error);
        }
    });
    $(document).on('click', "#deleteStudent", async function () {
        try {
            if (confirm('Are you sure delete?')) {
                let idBot = $(this).data('id')
                SignalApi.Student.deleteStudent({
                    Id: idBot,
                }).then((data) => {
                    if (!data.status) {
                        console.log(data.message);
                        alert(data.message)
                        return;
                    }
                    if (data.status) {
                        socket.emit('newStudentDeleted', data);
                        loadPage();
                    }
                })
            }
        } catch (error) {
            console.log(error)
        }
    });

    $('#modalEdit').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var code = button.data('code')
        var name = button.data('name')
        var birth = button.data('birth')
        let formattedDate = moment(birth).format("YYYY-MM-DD");
        var image = button.data('image')
        var modal = $(this)
        modal.find('#edit-studentId').val(id);
        modal.find('#edit-studentCode').val(code);
        modal.find('#edit-studentName').val(name);
        modal.find('#edit-studentBirth').val(formattedDate);
        modal.find('#edit-studentImg').attr("src", `http://localhost:7000${image}`);

    })
}


window.addEventListener('DOMContentLoaded', initialize)
