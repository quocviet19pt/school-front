$('.ajaxSumitForm').on('submit', function (e) {
    e.preventDefault();

    var disabled = $(this).find(':input:disabled').removeAttr('disabled');
    var dataForm = $(this).serializeArray();
    if (submitActor) {
        dataForm.push({
            'name': 'ActionSubmit',
            'value': $(submitActor).val()
        });
    }
    disabled.attr('disabled', 'disabled');
    var action = $(this).attr('action');
    if ($(this).valid()) {
        $.ajax({
            url: action,
            data: dataForm,
            type: 'POST',
            success: function (data) {
                formSuccess(data);
            },
            error: function (e) {
                console.log(e.message);
                formError(e);
            }
        });
    }
});

formSuccess = (data) => {

}

formError = (error) => {

}