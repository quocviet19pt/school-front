import { ajax, cookie } from "./signal_common"
export const SignalApi = {
    // student
    Student: {
        getStudent: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/student/get/search",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); },
                })
            });
        },

        addStudent: function (request) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.postFormData({
                    data: request,
                    url: "/api/api/student/create",
                    headers: {
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); }
                })
            });
        },

        updateStudent: function (request) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.postFormData({
                    data: request,
                    url: "/api/api/student/update",
                    headers: {
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); }
                })
            });
        },

        deleteStudent: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/student/delete",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); }
                })
            });
        },
    },

    // teacher
    Teacher: {
        getTeacher: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/teacher/get/search",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); },
                })
            });
        },

        addTeacher: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/teacher/create",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); }
                })
            });
        },
        updateTeacher: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/teacher/update",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); }
                })
            });
        },

        deleteTeacher: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/teacher/delete",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); }
                })
            });
        },
    },

    //subject
    Subject: {
        getSubject: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/subject/get/search",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); },
                })
            });
        },

        addSubject: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/subject/create",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); }
                })
            });
        },
        updateSubject: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/subject/update",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); }
                })
            });
        },

        deleteSubject: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/subject/delete",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); }
                })
            });
        },
    },

    //score
    StudentTest: {
        getStudentTest: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/student_test/get/search",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); },
                })
            });
        },

        addStudentTest: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/student_test/create",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); }
                })
            });
        },
        updateStudentTest: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/student_test/update",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); }
                })
            });
        },

        deleteStudentTest: function (request = {

        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/student_test/delete",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); }
                })
            });
        },
    },
    //test
    Test: {
        getTest: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/test/get/search",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); },
                })
            });
        },
        addTest: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/test/create",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); }
                })
            });
        },
        updateTest: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/test/update",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); }
                })
            });
        },

        deleteTest: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/test/delete",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); }
                })
            });
        },
    },
    //user
    User: {
        getUser: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/user/get",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); },
                })
            });
        },
        addUser: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/user/create",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); }
                })
            });
        },
        updateUser: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/user/update",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); }
                })
            });
        },

        deleteUser: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/user/delete",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); }
                })
            });
        },
    },
    //login
    checkLogin: {
        login: function (request = {
        }) {
            let token = cookie.getCookie("JWT")
            return new Promise(async function (resolve, reject) {
                ajax.post({
                    data: JSON.stringify(request),
                    url: "/api/api/login",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + token
                    },
                    success(data) { resolve(data) },
                    error(err) { reject(err); }
                })
            });
        },
    },
}