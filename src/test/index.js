import "../styleLoad";
import { submitForm } from "../core/signal_common";
import { SignalApi } from "../core/signal_api";
import io from "socket.io-client"
const body = document.querySelector('body'),
    sidebar = body.querySelector('nav'),
    toggle = body.querySelector(".toggle");
toggle.addEventListener("click", () => {
    sidebar.classList.toggle("close");
})

const initialize = async () => {
    loadPage();
    const socket = io.connect("http://localhost:7000");
    socket.on('testAdded', () => {
        loadPage();
        console.log("add thanh cong")
    });
    socket.on('testEdited', () => {
        loadPage();
        console.log("edit thanh cong")
    });
    socket.on('testDeleted', () => {
        loadPage();
        console.log("delete thanh cong")
    });
    function loadPage() {
        let codeSearch = $("#testCode-Search").val();
        let nameSearch = $("#testName-Search").val();
        SignalApi.Test.getTest(
            {
                Code: codeSearch,
                Name: nameSearch,
            }
        ).then((data) => {
            var template = $("#testTemplate").html()
            var div = ""
            for (const [index, obj] of data.data.entries()) {
                div += template.interpolate({
                    stt: index + 1,
                    id: obj.Id,
                    code: obj.Code,
                    name: obj.Name,
                    teacherId: obj.Teachers ? obj.Teachers.Id : "",
                })
            }
            $("#showData").html(div)
        })
            .catch((error) => {
                console.log(error)
            })
    }
    $(document).on('click', "#searchTest", async function () {
        loadPage()
    })


    $(document).on('click', "#addTest", async function () {
        try {
            var addTest = () => {

                let code = $("#add-testCode").val();
                let name = $("#add-testName").val();
                let teacherId = $("#add-testTeacherId").val();

                SignalApi.Test.addTest({
                    Code: code,
                    Name: name,
                    TeacherId: teacherId
                }).then((data) => {
                    if (!data.status) {
                        alert(data.message);
                        return;
                    }
                    if (data.status) {
                        $("#modalAdd").modal('hide');
                        $("#add-testCode").val("");
                        $("#add-testName").val("");
                        $("#add-testTeacherId").val("");
                        socket.emit('newTestAdded', data);
                        loadPage();
                    }
                });
            };
            submitForm(addTest);
        } catch (error) {
            console.log(error);
        }
    });


    $(document).on('click', "#editTest", async function () {
        try {
            var editTest = () => {
                let id = $("#edit-testId").val();
                let code = $("#edit-testCode").val();
                let name = $("#edit-testName").val();
                let teacherId = $("#edit-testTeacherId").val();

                SignalApi.Test.updateTest({
                    Id: id,
                    Code: code,
                    Name: name,
                    TeacherId: teacherId
                }).then((data) => {
                    if (!data.status) {
                        alert(data.message);
                        return;
                    }
                    if (data.status) {
                        $("#modalEdit").modal('hide');
                        socket.emit('newTestEdited', data);
                        loadPage();
                    }
                });
            };
            submitForm(editTest);
        } catch (error) {
            console.log(error);
        }
    });
    $(document).on('click', "#deleteTest", async function () {
        try {
            if (confirm('Are you sure delete?')) {
                let idBot = $(this).data('id')
                SignalApi.Test.deleteTest({
                    Id: idBot,
                }).then((data) => {
                    if (!data.status) {
                        console.log(data.message);
                        alert(data.message)
                        return;
                    }
                    if (data.status) {
                        socket.emit('newTestDeleted', data);
                        loadPage();
                    }
                })
            }
        } catch (error) {
            console.log(error)
        }
    });

    $('#modalEdit').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var code = button.data('code')
        var name = button.data('name')
        var teacherId = button.data('teacherid')
        var modal = $(this)
        modal.find('#edit-testId').val(id);
        modal.find('#edit-testCode').val(code);
        modal.find('#edit-testName').val(name);
        modal.find('#edit-testTeacherId').val(teacherId)
    })
}


window.addEventListener('DOMContentLoaded', initialize)
