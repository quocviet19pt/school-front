import { cookie, getUserRole, checkTimeOutJwt, authentication } from "../core/signal_common";
import "../styleLoad";
let JWT = cookie.getCookie("JWT")

const initialize = async () => {
    if (!JWT) {
        window.location = "/"
    }
    let checkTimeOut = checkTimeOutJwt(JWT)
    if (!checkTimeOut) {
        let JWT = ""
        cookie.setCookie("JWT", JWT)
        window.location = "/"
    }
    let userRole = getUserRole(JWT)
    if (userRole == 'User') {
        $('#menuUser').show()
    }
    if (userRole == 'Admin') {
        $('#menuAdmin').show()
    }
    let path = String(window.location.pathname)
    var authData = {
        "/telegramBot": { Admin: ".telegramBot" },
        "/telegramBotGroup": { Admin: ".telegramBotGroup" },
        "/telegramBotManual": { Admin: ".telegramBotManual" },
        "/pumpDetectionSetting": { Admin: ".pumpDetectionSetting" },
        "/pumpDetectionRequest": { Admin: ".pumpDetectionRequest" },
        "/pumpDetectionHistory": { Admin: ".pumpDetectionHistory" },
        "/pumpPerformanceReport": { Admin: ".pumpPerformanceReport" },
        "/manageUsers": { Admin: ".manageUsers" },
        "/pumpDetectionCustomerRequests": {
            Admin: ".pumpDetectionCustomerRequests",
            User: ".pumpDetectionCustomerRequests"
        },
        "/pumpPerformanceCustomerReport": {
            Admin: ".pumpPerformanceCustomerReport",
            User: ".pumpPerformanceCustomerReport"
        },
        "/manageUsers": {
            Admin: ".manageUsers",
            User: ".manageUsers"
        }
    }
    var classAuth = authData[path][userRole]
    if (classAuth) $(classAuth).addClass("active")
    else authentication(userRole)

    $("#logout").click(function () {
        let JWT = ""
        cookie.setCookie("JWT", JWT)
        window.location = "/"
    })
}
window.addEventListener('DOMContentLoaded', initialize)