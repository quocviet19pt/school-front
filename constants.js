const dotenv = require("dotenv");
const path = require('path');

dotenv.config({
    path: path.resolve(__dirname, '.env.' + process.env.Environment)
});

exports.Port = process.env.Port;
exports.PortApi = process.env.PortApi;
exports.IsProd = process.env.Environment == "prod"
