import { SignalApi } from "./core/signal_api";
import { submitForm, cookie, getUserRole } from "./core/signal_common";
import "./styleLoad";

const initialize = async () => {
    // check login
    $("#login").click(function () {
        try {
            var login = () => {
                let username = $('#username').val()
                let password = $('#password').val()
                SignalApi.checkLogin.login(
                    {
                        Name: username,
                        Password: password,
                    }
                ).then((data) => {
                    if (data.status) {
                        cookie.setCookie("JWT", data.token)
                        let JWT = cookie.getCookie("JWT")
                        let userRole = getUserRole(JWT)
                        if (userRole == 'User') {
                            window.location = "./homepage"
                        }
                        if (userRole == 'Admin') {
                            window.location = "./homepage"
                        }
                    } else {
                        alert(data.message)
                    }
                }).catch((error) => {
                    console.log(error)
                })
            }
            submitForm(login);
        } catch (error) {
            console.log(error)
        }
    })
}

window.addEventListener('DOMContentLoaded', initialize)