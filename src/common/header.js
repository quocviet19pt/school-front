$(document).ready(function () {
    $(".nav-link-menu").click(function () {
        $(".main-sidebar").toggleClass('active');
    });

    const $menu = $('.main-sidebar');

    $(document).mouseup(e => {
        if (!$menu.is(e.target)
            && $menu.has(e.target).length === 0) {
            $menu.removeClass('active');
        }
    });

})