import "../styleLoad";
import { submitForm } from "../core/signal_common";
import { SignalApi } from "../core/signal_api";
import io from "socket.io-client"
const body = document.querySelector('body'),
    sidebar = body.querySelector('nav'),
    toggle = body.querySelector(".toggle");
toggle.addEventListener("click", () => {
    sidebar.classList.toggle("close");
})

const initialize = async () => {
    loadPage()
    const socket = io.connect("http://localhost:7000");
    socket.on('userAdded', () => {
        loadPage();
        console.log("add thanh cong")
    });
    socket.on('userEdited', () => {
        loadPage();
        console.log("edit thanh cong")
    });
    socket.on('userDeleted', () => {
        loadPage();
        console.log("delete thanh cong")
    });
    function loadPage() {
        let nameSearch = $("#userName-Search").val();
        let roleSearch = $("#userRole-Search").val();
        SignalApi.User.getUser(
            {
                Name: nameSearch,
                Role: roleSearch,
            }
        ).then((data) => {
            var template = $("#userTemplate").html()
            var div = ""
            for (const [index, obj] of data.data.entries()) {
                div += template.interpolate({
                    stt: index + 1,
                    id: obj.Id,
                    name: obj.Name,
                    role: obj.Role,
                    password: obj.Password,
                })
            }
            $("#showData").html(div)
        })
            .catch((error) => {
                console.log(error)
            })
    }
    $(document).on('click', "#searchUser", async function () {
        loadPage()
    })


    $(document).on('click', "#addUser", async function () {
        try {
            var addUser = () => {

                let name = $("#add-userName").val();
                let role = $("#add-userRole").val();
                let password = $("#add-userPassword").val();

                SignalApi.User.addUser({
                    Name: name,
                    Role: role,
                    Password: password
                }).then((data) => {
                    if (!data.status) {
                        alert(data.message);
                        return;
                    }
                    if (data.status) {
                        $("#modalAdd").modal('hide');
                        $("#add-userName").val("");
                        $("#add-userRole").val("");
                        $("#add-userPassword").val("");
                        socket.emit('newUserAdded', data);
                        loadPage();
                    }
                });
            };
            submitForm(addUser);
        } catch (error) {
            console.log(error);
        }
    });


    $(document).on('click', "#editUser", async function () {
        try {
            var editUser = () => {
                let id = $("#edit-userId").val();
                let name = $("#edit-userName").val();
                let role = $("#edit-userRole").val();
                let password = $("#edit-userPassword").val();

                SignalApi.User.updateUser({
                    Id: id,
                    Name: name,
                    Role: role,
                    Password: password
                }).then((data) => {
                    if (!data.status) {
                        alert(data.message);
                        return;
                    }
                    if (data.status) {
                        $("#modalEdit").modal('hide');
                        socket.emit('newUserEdited', data);
                        loadPage();
                    }
                });
            };
            submitForm(editUser);
        } catch (error) {
            console.log(error);
        }
    });
    $(document).on('click', "#deleteUser", async function () {
        try {
            if (confirm('Are you sure delete?')) {
                let idBot = $(this).data('id')
                SignalApi.User.deleteUser({
                    Id: idBot,
                }).then((data) => {
                    if (!data.status) {
                        console.log(data.message);
                        alert(data.message)
                        return;
                    }
                    if (data.status) {
                        socket.emit('newUserDeleted', data);
                        loadPage();
                    }
                })
            }
        } catch (error) {
            console.log(error)
        }
    });

    $('#modalEdit').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var name = button.data('name')
        var role = button.data('role')
        var password = button.data('password')
        var modal = $(this)
        modal.find('#edit-userId').val(id);
        modal.find('#edit-userName').val(name);
        modal.find('#edit-userRole').val(role);
        modal.find('#edit-userPassword').val(password)
    })
}


window.addEventListener('DOMContentLoaded', initialize)
