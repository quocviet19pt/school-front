import "../styleLoad";
import { submitForm } from "../core/signal_common";
import { SignalApi } from "../core/signal_api";
import io from "socket.io-client"
const body = document.querySelector('body'),
    sidebar = body.querySelector('nav'),
    toggle = body.querySelector(".toggle");
toggle.addEventListener("click", () => {
    sidebar.classList.toggle("close");
})

const initialize = async () => {
    loadPage();
    const socket = io.connect("http://localhost:7000");
    socket.on('subjectAdded', () => {
        loadPage();
        console.log("add thanh cong")
    });
    socket.on('subjectEdited', () => {
        loadPage();
        console.log("edit thanh cong")
    });
    socket.on('subjectDeleted', () => {
        loadPage();
        console.log("delete thanh cong")
    });
    function loadPage() {
        let codeSearch = $("#subjectCode-Search").val();
        let nameSearch = $("#subjectName-Search").val();
        SignalApi.Subject.getSubject(
            {
                Code: codeSearch,
                Name: nameSearch,
            }
        ).then((data) => {
            var template = $("#subjectTemplate").html()
            var div = ""
            for (const [index, obj] of data.data.entries()) {
                div += template.interpolate({
                    stt: index + 1,
                    id: obj.Id,
                    code: obj.Code,
                    name: obj.Name,
                })
            }
            $("#showData").html(div)
        })
            .catch((error) => {
                console.log(error)
            })
    }
    $(document).on('click', "#searchSubject", async function () {
        loadPage()
    })


    $(document).on('click', "#addSubject", async function () {
        try {
            var addSubject = () => {

                let code = $("#add-subjectCode").val();
                let name = $("#add-subjectName").val();

                SignalApi.Subject.addSubject({
                    Code: code,
                    Name: name,
                }).then((data) => {
                    if (!data.status) {
                        alert(data.message);
                        return;
                    }
                    if (data.status) {
                        $("#modalAdd").modal('hide');
                        $("#add-subjectCode").val("");
                        $("#add-subjectName").val("");
                        socket.emit('newSubjectAdded', data);
                        loadPage();
                    }
                });
            };
            submitForm(addSubject);
        } catch (error) {
            console.log(error);
        }
    });


    $(document).on('click', "#editSubject", async function () {
        try {
            var editSubject = () => {
                let id = $("#edit-subjectId").val();
                let code = $("#edit-subjectCode").val();
                let name = $("#edit-subjectName").val();

                SignalApi.Subject.updateSubject({
                    Id: id,
                    Code: code,
                    Name: name,
                }).then((data) => {
                    if (!data.status) {
                        console.log(data)
                    }
                    if (data.status) {
                        console.log(data)
                        $("#modalEdit").modal('hide');
                        socket.emit('newSubjectEdited', data);
                        loadPage();
                    }
                });
            };
            submitForm(editSubject);
        } catch (error) {
            console.log(error);
        }
    });
    $(document).on('click', "#deleteSubject", async function () {
        try {
            if (confirm('Are you sure delete?')) {
                let idBot = $(this).data('id')
                SignalApi.Subject.deleteSubject({
                    Id: idBot,
                }).then((data) => {
                    if (!data.status) {
                        console.log(data.message);
                        alert(data.message)
                        return;
                    }
                    if (data.status) {
                        socket.emit('newSubjectDeleted', data);
                        loadPage();
                    }
                })
            }
        } catch (error) {
            console.log(error)
        }
    });

    $('#modalEdit').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var code = button.data('code')
        var name = button.data('name')
        var modal = $(this)
        modal.find('#edit-subjectId').val(id);
        modal.find('#edit-subjectCode').val(code);
        modal.find('#edit-subjectName').val(name);
    })
}


window.addEventListener('DOMContentLoaded', initialize)
