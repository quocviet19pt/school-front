const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const fs = require('fs');
const { Port, PortApi } = require('./constants')
const DIST = path.resolve(__dirname, 'dist')

module.exports = {
    entry: {
        main: "./src/index.js",
        homepage: "./src/homepage/index.js",
        student: "./src/student/index.js",
        teacher: "./src/teacher/index.js",
        subject: "./src/subject/index.js",
        test: "./src/test/index.js",
        scores: "./src/scores/index.js",
        user: "./src/user/index.js",
        style: "./src/styleLoad.js",
    },
    output: {
        filename: (data) => {
            if (data.chunk.name) {
                switch (data.chunk.name) {
                    case "index":
                        return `index.[fullhash].js`
                    default:
                        return `[name]/index.[fullhash].js`
                }
            }
            fileNameIndex++;
            return `vendor/other${fileNameIndex - 1 == 0 ? "" : fileNameIndex - 1}.[fullhash].js`
        },
        path: DIST,
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            url: false,
                        }
                    }
                ],
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            inject: "body",
            chunks: ['main'],
            filename: 'index.html'
        }),
        new HtmlWebpackPlugin({
            template: "./src/homepage/index.html",
            inject: "body",
            chunks: ['homepage'],
            filename: 'homepage/index.html'
        }),
        new HtmlWebpackPlugin({
            template: "./src/student/index.html",
            inject: "body",
            chunks: ['student'],
            filename: 'student/index.html'
        }),
        new HtmlWebpackPlugin({
            template: "./src/teacher/index.html",
            inject: "body",
            chunks: ['teacher'],
            filename: 'teacher/index.html'
        }),
        new HtmlWebpackPlugin({
            template: "./src/subject/index.html",
            inject: "body",
            chunks: ['subject'],
            filename: 'subject/index.html'
        }),
        new HtmlWebpackPlugin({
            template: "./src/test/index.html",
            inject: "body",
            chunks: ['test'],
            filename: 'test/index.html'
        }),
        new HtmlWebpackPlugin({
            template: "./src/scores/index.html",
            inject: "body",
            chunks: ['scores'],
            filename: 'scores/index.html'
        }),
        new HtmlWebpackPlugin({
            template: "./src/user/index.html",
            inject: "body",
            chunks: ['user'],
            filename: 'user/index.html'
        }),

        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({ filename: "assets/css/[name].[contenthash].css" }),
        new CopyWebpackPlugin({
            patterns: [
                {
                    from: "./src/",
                    globOptions: {
                        ignore: ['**/*.html', '**/*.css'],
                    }
                },
            ],
        }),
    ],
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
            chunks: 'all',
            maxInitialRequests: Infinity,
            minSize: 0,
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name(module) {
                        // get the name. E.g. node_modules/packageName/not/this/part.js
                        // or node_modules/packageName
                        const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];

                        // npm package names are URL-safe, but some servers don't like @ symbols
                        return `vendor/npm.${packageName.replace('@', '')}`;
                    },
                },
            },
        },
        minimizer: [
            new CssMinimizerPlugin(),
        ]
    },
    performance: {
        hints: false,
        maxEntrypointSize: 512000,
        maxAssetSize: 512000
    },
    devServer: {
        allowedHosts: "all",
        devMiddleware: {
            writeToDisk: true
        },
        static: {
            directory: path.join(__dirname, 'dist'),
        },
        port: Port,
        proxy: {
            '/api': {
                target: `http://localhost:${PortApi}`,
                changeOrigin: true,
                pathRewrite: {
                    '^/api': ''
                }
            }
        }
    },
};